import React from 'react';
import './App.css';
import unfetch from 'unfetch';

// Fix a bug making unfetch not being properly bound with webpack.
const fetch = unfetch;

function App() {
    return (
      <div id="login">
        <h1>SIGN IN<span className="orangestop">.</span></h1>
        <span className="input">
          <span className="icon username-icon fontawesome-user" />
          <input type="text" className="username" placeholder="Username" />
        </span>
        <span className="input">
          <span className="password-icon-style icon password-icon fontawesome-lock" />
          <input type="password" className="password" placeholder="Password" />
        </span>
        <div className="forgot">Forgot Details?</div>
        <div className="divider" />
        <button onClick = {() => {SigIn()}}>Log In</button>
        <p>Don't have an account? 
          <span className="reg">Register.</span>
        </p>
      </div>
  );
}

function SigIn(){

  var username = document.getElementsByClassName('username')[0].value;
  var password = document.getElementsByClassName('password')[0].value;
  const data = { UserID: username, Password: password }
  const url = 'http://localhost:5000/api/login';
  
  fetch(url,{
    method:'POST',
    //mode:'cors',
    credentials: 'omit',
    headers:{
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': '*'
    },
    body:JSON.stringify(data)
  }).then(function (response) {

    //Check if response is 200(OK) 
    if(response.ok) {
      alert("welcome ");
    }
    //if not throw an error to be handled in catch block
    throw new Error(response);
  })
  .catch(function (error) {
    //Handle error
    console.log(error);
  });
}

export default App;
