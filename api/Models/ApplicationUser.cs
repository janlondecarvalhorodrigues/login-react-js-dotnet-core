﻿using Microsoft.AspNetCore.Identity;

namespace apiLogin.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}